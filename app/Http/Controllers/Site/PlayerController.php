<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Player;

class PlayerController extends Controller
{
    public function updateOrCreate(Request $request){
            //dd(auth()->user()->id);
        $player = Player::updateOrCreate(
            ['user_id' => auth()->user()->id],
            [
                'nickname'      => $request->nickname,
                'genre'         => $request->genre,
                'birth_date'    => $request->birth_date,
                'position'      => $request->position,
                'age'           => $request->age,
                'contact_phone' => $request->contact_phone,
                'bio'           => $request->bio,
                'facebook'      => $request->facebook,
                'instagram'     => $request->instagram,
                'user_id'       => auth()->user()->id
            ]
           
        );

        if($player){
            return redirect()->back();
        }else{
            return 'erro ao cadastrar player';
        }
    }
}
