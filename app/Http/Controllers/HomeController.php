<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Player;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $players = Player::all();
        $player  = Player::find(auth()->user()->id);
        
        return view('home',[
            'players'   => $players,
            'player'    => $player ? $player : null
        ]);
    }
}
