<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
   protected $fillable = ['nickname','position','age','contact_phone','bio','avatar','facebook','instagran','bith_date','genre','user_id'];
}
