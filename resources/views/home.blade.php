@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4 col-sm-12">
        
        <div class="text-center">
            <a href="#" class="btn btn-outline-success"  data-toggle="modal" data-target="#exampleModal">Atualizar Perfil</a>
        </div>
        <br>
        
            <div class="card">
                <div class="avatar text-center">
                    <img class="card-img-top text-center" src="{{asset('images/avatar.png')}}" style="height:150px; width:150px" alt="Card image cap">
                </div>
                    <div class="card-body">
                        <h5 class="card-title">{{auth()->user()->name}} - {{ $player->nickname ?? '' }}</h5>
                        <p class="card-text"> {{ $player->bio ?? ''}}</p>
                    </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">Sexo: {{ $player->genre ?? '' }}</li>
                    <li class="list-group-item">Posição Principal :{{ $player->position ?? '' }}</li>
                    <li class="list-group-item">Idade :{{ $player->age ?? ''}}</li>
                </ul>                
            </div>
        </div>

        <div class="col-md-4 col-sm-12">
        <p>Em destaque</p>
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                        <img class="d-block w-100" src="{{asset('images/slider3.png')}}" alt="First slide">
                        </div>
                        <div class="carousel-item">
                        <img class="d-block w-100" src="{{asset('images/slider3.png')}}" alt="Second slide">
                        </div>
                        <div class="carousel-item">
                        <img class="d-block w-100" src="{{asset('images/slider3.png')}}" alt="Third slide">
                        </div>
                    </div>
                    
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
            </div>
        </div>

        <div class="col-md-4 col-sm-12">
        <p>Quem já se cadastrou no Driblei</p>
            
            <div class="list-group">
                @forelse($players as $p)
                <a href="#" class="list-group-item list-group-item-action flex-column align-items-start active-success">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">{{$p->nickname}}</h5>
                        <small>3 days ago</small>
                    </div>
                    <p class="mb-1">{{$p->position}}</p>                    
                </a>
                @empty
                <p>Ninguem se cadastrou ainda :(</p>
                @endforelse                
            </div>            
        </div>
    </div>    
</div>

<!-- Modal for player -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Informações do seu Perfil</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('player.update')}}" method="POST">
        <div class="modal-body">        
            @csrf 
                <div class="form-group">
                    <label for="Apelido">Apelido</label>
                    <input type="text" name="nickname" class=form-control>
                </div>
                <div class="form-group">
                    <label for="Nascimento">Nascimento</label>
                    <input type="date" name="birth_date" class=form-control>
                </div>
                
                <div class="row">
                    <div class="form-group col-sm-4">
                        <label for="Genero">Genero</label>
                        <select name="genre" id="" class="form-control">
                            <option value="Masculino">Masculino</option>
                            <option value="Feminino">Feminino</option>
                            <option value="Outro">Outro</option>
                        </select>
                    </div>
                    <div class="form-group col-sm-4">
                        <label for="Apelino">Posição Principal</label>
                        <select name="position" id="" class="form-control">
                            <option value="LD">Lateral Direito</option>
                            <option value="AT">Atacante</option>
                            <option value="GO">Goleiro</option>
                        </select>
                    </div>
                    <div class="form-group col-sm-4">
                        <label for="Idade">Idade</label>
                        <input type="text" name="age" class=form-control>
                    </div>
                </div>               
       
      </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-success">Atualizar Perfil</button>
        </div>
    </div>
    </form>
  </div>
</div>
@endsection
